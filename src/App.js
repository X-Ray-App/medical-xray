import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import {} from "react-bootstrap"
import {Navigator} from "./component/Navigator";
import { createBrowserHistory } from "history";
import Login from "./page/Login";
import Home from './page/Home';
const history = createBrowserHistory();
function App() {
  return (    
    <Router history={history}>    
    <div className="main-gradient">
      <Navigator ></Navigator>      
      <div className="main-container w-container">
      <Route path="/" exact component={Login} />
      <Route exact path="/Login/" component={Login} />
      <Route exact path="/Home/" component={Home} />
      </div>
      <div className="copyright">© 2019 Hip &amp; Knee - Step by Step. All rights reserved.</div>
    </div>
    </Router>
  );
}

export default App;
