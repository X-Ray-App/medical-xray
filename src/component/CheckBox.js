import React from "react";
export class CheckBox extends React.Component{
    render(){
        return (
            <div className="photo-div">
                <div className={`white-circle ${this.props && this.props.checked ? "gray":"" }`}>
                {this.props && this.props.checked && (<img src={require("../assets/img/5d7986f8a637b348ccb2fdc8_5d261cbd528d2e4c51c05642_noun_Check_2422525_ffffff.png")} 
                            srcSet="https://assets.website-files.com/5d78fef28f5b3e5ef3a9cec9/5d7986f8a637b348ccb2fdc8_5d261cbd528d2e4c51c05642_noun_Check_2422525_ffffff-p-500.png 500w,
                             https://assets.website-files.com/5d78fef28f5b3e5ef3a9cec9/5d7986f8a637b348ccb2fdc8_5d261cbd528d2e4c51c05642_noun_Check_2422525_ffffff.png 506w" 
                             sizes="(max-width: 767px) 26.02272605895996px, (max-width: 991px) 3vw, 26.02272605895996px" 
                             alt="" className="fade" /> )}
                </div>                
            </div>
        );
    }
}