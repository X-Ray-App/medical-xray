import React from "react";
import { Dropdown }  from "react-bootstrap";

export class Navigator extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            menu:false
        };
    }
    render(){
        return (                           
                <div className="navbar w-nav" onClick={()=>this.setState({menu: !this.state.menu})}>
                    <div className={`menu-button w-nav-button ${this.state.menu? "w--open":""}`}>
                        <div className="icon w-icon-nav-menu"></div>
                    </div>
                    <div className={`w-nav-overlay ${this.state.menu? "nav-menu-open": "nav-menu-close"}`}>
                    <nav role="navigation" className="nav-menu w-nav-menu w--nav-menu-open">
                        <a href="#" className="nav-link w-nav-link w--nav-link-open">Right Knee Sample Patient</a>
                        <div className="neon-green-line tiny center"></div>
                        <a href="#" className="nav-link w-nav-link w--nav-link-open">Left Knee Sample Patient</a>
                        <div className="neon-green-line tiny center"></div>
                        <a href="#" className="nav-link w-nav-link w--nav-link-open">Right Hip Sample Patient</a>
                        <div className="neon-green-line tiny center"></div>
                        <a href="#" className="nav-link w-nav-link w--nav-link-open">Left Hip Sample Patient</a>
                    </nav>
                    </div>
                </div>
        );
    }

}