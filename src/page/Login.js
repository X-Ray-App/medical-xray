import React, {Component} from "react";

export default class Login extends Component{
    render(){
        return (
            <div className="w-row">
                <div className="w-col w-col-8">
                    <div className="container"></div>
                    <h1 className="heading">Hip &amp; Knee <br/>Step by Step</h1>
                    <div className="div2"></div>
                    <div className="top-header"></div>
                    <div className="w-form">
                        <form id="email-form" name="email-form" data-name="Email Form" className="form-5 w-clearfix">
                            <div className="div-block-2">
                                <label className="field-label">User ID:</label>
                                <input type="text" className="text-field w-input" maxLength="256" name="name" id="name" />                                    
                            </div>
                            <div className="div-block-2">
                                <label className="field-label">Password:</label>
                                <input type="text" className="text-field w-input" maxLength="256" name="name-2" id="name-2" />
                            </div>
                            <a href="/sbs-x-ray-review" className="button-confirm form w-button">Login</a>
                            <input type="submit" value="Login" className="button-confirm form hide w-button" />
                        </form>
                        <div className="w-form-done">
                            <div>Thank you! Your submission has been received!</div>
                        </div>
                        <div className="w-form-fail">
                            <div>Oops! Something went wrong while submitting the form.</div>
                        </div>
                    </div>
                </div>
                <div className="w-col w-col-4">
                    <img src="https://assets.website-files.com/5d78fef28f5b3e5ef3a9cec9/5d78fef28f5b3e197ba9ceef_Bitmap.png" alt="" className="image" />
                </div>
            </div>

        );
    }
}