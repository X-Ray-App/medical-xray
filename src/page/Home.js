import React, {Component} from "react";
import { CheckBox } from "../component/CheckBox";
import {Note} from "../component/Note";
import {Modal} from "react-bootstrap";

export default class Home extends Component{
    constructor(props){
        super(props);
        this.state={
            addNote:false
        }
    }
    render (){
        return (
        <section>
            <div className="columns-6 w-row">
                <div className="column-right-padding w-col w-col-7">
                    <h3 className="heading-4">SBS X-ray Review</h3>
                </div>
                <div className="w-col w-col-5">
                    <p className="small-paragraph">X-ray Name: 4875038 1X1.jpg</p>
                </div>
            </div>
            <img src="https://assets.website-files.com/5d78fef28f5b3e5ef3a9cec9/5d79021d765b1b21bf5f0aa8_Asset%201PROCESS.png" 
            srcSet="https://assets.website-files.com/5d78fef28f5b3e5ef3a9cec9/5d79021d765b1b21bf5f0aa8_Asset%201PROCESS-p-500.png 500w,
             https://assets.website-files.com/5d78fef28f5b3e5ef3a9cec9/5d79021d765b1b21bf5f0aa8_Asset%201PROCESS-p-800.png 800w,
              https://assets.website-files.com/5d78fef28f5b3e5ef3a9cec9/5d79021d765b1b21bf5f0aa8_Asset%201PROCESS.png
               938w" sizes="(max-width: 479px) 84vw, (max-width: 767px) 85vw, (max-width: 991px) 652.9971313476562px, 874.5880126953125px" alt="" className="big-xray"/>

               <table className="tablet">
                   <tbody>
                   <tr>
                       <td></td>
                       <td>
                            <p className="table-text c">Right Lateral</p>
                        </td>
                       <td >
                            <p className="table-text c">Right Medial</p>
                       </td>
                       <td>
                            <p className="table-text c">Left Medial</p>
                        </td>
                       <td>
                            <p className="table-text c">Left Lateral</p>
                        </td>
                       <td>
                           <a  className="view-report button-7 w-button"
                            onClick={()=>this.setState({addNote:true})}>X-ray Error</a>                          
                        </td>
                   </tr>
                   <tr>
                       <td style={{verticalAlign:"bottom"}}>
                           <p className="table-text">Normal/Slight</p>
                        </td>
                       <td><CheckBox/></td>
                       <td><CheckBox/></td>
                       <td><CheckBox/></td>
                       <td><CheckBox/></td>
                       <td></td>
                   </tr>
                   <tr>
                       <td style={{verticalAlign:"bottom"}}>
                            <p className="table-text">Moderate</p>
                       </td>
                       <td><CheckBox/></td>
                       <td><CheckBox/></td>
                       <td><CheckBox/></td>
                       <td><CheckBox checked={true}/></td>
                       <td></td>
                   </tr>
                   <tr>
                       <td style={{verticalAlign:"bottom"}}>
                            <p className="table-text">NES</p>
                        </td>
                       <td><CheckBox checked={true}/></td>
                       <td><CheckBox/></td>
                       <td><CheckBox/></td>
                       <td><CheckBox/></td>                       
                       <td></td>
                   </tr>
                   <tr>
                       <td style={{verticalAlign:"bottom"}}>
                            <p className="table-text">End Stage</p>
                        </td>
                       <td><CheckBox/></td>
                       <td><CheckBox checked={true}/></td>
                       <td><CheckBox/></td>
                       <td><CheckBox/></td>
                       <td>
                        <a href="#" className="view-report button-7 green w-button" 
                            onClick={()=>this.setState({addNote:true})}>Add Note</a>
                       </td>
                   </tr>
                   </tbody>
               </table>

               <div className=" w-row">
                   <div className="column-right-padding w-col w-col-7">
                       <a href="/" className="button-confirm w-button">Back</a>
                    </div>
                    <div className="column-4 w-col w-col-5">
                        <a href="#" className="button-confirm w-button">Next</a>
                    </div>
                </div>
                <Note show={this.state.addNote} onSubmit={()=>this.setState({addNote:false})} onCancel={()=>this.setState({addNote:false})} />             
        </section>
        );
    }
}